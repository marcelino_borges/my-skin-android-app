package devbox.com.myskin.models;

/**
 * Created by Marcelino Borges on 11/06/2018 at 13:15.
 */

public class ConfigsUser {
    private String idUser;
    //atribute of alarmSound
    private Theme theme;

    public ConfigsUser() {
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }
}
