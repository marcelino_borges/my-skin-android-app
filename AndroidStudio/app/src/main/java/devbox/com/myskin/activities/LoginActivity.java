package devbox.com.myskin.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import devbox.com.myskin.R;
import devbox.com.myskin.helpers.Preferences;
import devbox.com.myskin.models.User;

/**
 * Created by Marcelino Borges on 10/06/2018 at 12:31.
 */
public class LoginActivity extends AppCompatActivity {

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    // UI references
    private TextInputEditText mPasswordView, mEmailView;
    private TextInputLayout mPassword_inputLayout, mEmail_inputLayout;
    private SignInButton mButton_googleSignIn;
    private AlertDialog progressDialog;

    // Configure Google Sign In
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;

    private static final String NODE_USERS = "users";

    @Override
    protected void onStart() {
        super.onStart();

        //If user detected, loads MainActivity
        if (isUserLoggedIn()) {
            loadMainActivity();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Sets up Firebase Authentication instance
        mAuth = FirebaseAuth.getInstance();

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Finding login form components
        mEmailView = findViewById(R.id.txtEmailLogin);
        mPasswordView = findViewById(R.id.txtPasswordLogin);
        mEmail_inputLayout = findViewById(R.id.text_input_layout_signup_email);
        mPassword_inputLayout = findViewById(R.id.text_input_layout_login_senha);
        Button mLoginButton = findViewById(R.id.btnLogin);
        Button mSignUpButton = findViewById(R.id.btnSignUp);

        mButton_googleSignIn = findViewById(R.id.button_google_sign_in);

        //TODO: Adicionar "Esqueci a senha"

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    logUserIn();
                    return true;
                }
                return false;
            }
        });

        mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                logUserIn();
            }
        });

        mSignUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loadSignUpActivity();
            }
        });

        mButton_googleSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGoogle();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.e(getString(R.string.tag_log_error), "Google sign in failed > LoginActivity > onActivityResult()", e);
                // ...
            }
        }
    }

    ///////////////////////
    //                   //
    //  CUSTOM METHODS   // ___
    //                   //    |
    ///////////////////////    V

    private void signInWithGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(getString(R.string.tag_log_error), "firebaseAuthWithGoogle:" + acct.getId());

        showProgressDialog();

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(getString(R.string.tag_log_error), "signInWithCredential: success");

                        addGoogleUserToDatabase();

                        if(mAuth.getCurrentUser() != null) {
                            loadMainActivity();
                        }

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.e(getString(R.string.tag_log_error), "signInWithCredential: failure", task.getException());
                        showToast(getString(R.string.error_login_authentication));
                    }
                }
            });
    }

    private void addGoogleUserToDatabase() {
        //Retrieving database new reference at the database's root
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //Check if this user's node is already created in database
        if(mAuth.getCurrentUser() != null) {

            String uId = mAuth.getCurrentUser().getUid();

            mDatabase.child(NODE_USERS).child(uId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists() && dataSnapshot.getValue(User.class) == null) {
                        //If the user isn't already registered in Database
                        String name = mAuth.getCurrentUser().getDisplayName();
                        String email = mAuth.getCurrentUser().getEmail();

                        User newUser = new User(name, mAuth.getCurrentUser().getUid(), email);

                        /*
                         * If node "user" doesn't exist, creates a new one (only at the first time app runs)
                         * If node already exists, access it and create a new node with user's id and save the user object
                         */
                        mDatabase.child(NODE_USERS).child(newUser.getId()).setValue(newUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                showToast(getString(R.string.sign_up_successful));
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    //Signs user in from the form data
    public void logUserIn() {
        //Retrieving user data
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        //First cleans the possible errors in the fields
        mEmailView.setError(null);
        mEmail_inputLayout.setError(null);
        mPasswordView.setError(null);
        mPassword_inputLayout.setError(null);

        try {

            //Signs in to Firebase Auth
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        showProgressDialog();
                        try {
                            //Saving logged-in user's email and ID to preferences
                            Preferences preferences = new Preferences(LoginActivity.this);
                            preferences.saveUserInPreferences(mAuth.getCurrentUser().getEmail(), mAuth.getCurrentUser().getUid());
                        } catch (NullPointerException npe) {
                            Log.e("MY_SKIN ERROR: ", "E-mail não localizado no FirebaseAuth.getCurrentUser() para salvar nas preferências");
                        }
                        loadMainActivity();
                    } else {
                        //Treating possible exceptions
                        try {
                            throw task.getException();
                        } catch (FirebaseAuthInvalidUserException e) {
                            mEmailView.setError(getString(R.string.error_login_invalid_email));
                            mEmail_inputLayout.setError(getString(R.string.error_login_invalid_email));
                        } catch (FirebaseAuthInvalidCredentialsException e) {
                            mPasswordView.setError(getString(R.string.error_login_invalid_password));
                            mPassword_inputLayout.setError(getString(R.string.error_login_invalid_password));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (IllegalArgumentException e) {
             //Then sets the errors on the empty/null fields
            if (!hasData(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                mEmail_inputLayout.setError(getString(R.string.error_field_required));
            }
            if (!hasData(password)) {
                mPasswordView.setError(getString(R.string.error_field_required));
                mPassword_inputLayout.setError(getString(R.string.error_field_required));
            }
        }
    }

    private boolean hasData(String text) {
        return !TextUtils.isEmpty(text);
    }

    //Checks if there is an user logged-in
    public boolean isUserLoggedIn() {
        // Retrieving a possible logged-in user
        FirebaseUser currentUser = mAuth.getCurrentUser();

        return (currentUser != null);
    }

    private void showProgressDialog() {
        setContentView(R.layout.loading_screen);
//        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
//        builder.setView(R.layout.loading_screen);
//        progressDialog = builder.create();
//        progressDialog.show();
    }

    private void showToast(String msg) {
        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
    }

//    private void hideProgressBar() {
//        progressDialog.dismiss();
//    }

    ///////////////////////
    //                   //
    //  LOAD ACTIVITIES  // ___
    //                   //    |
    ///////////////////////    V

    //Loads MainActivity
    private void loadMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        //Closes current activity
        finish();
    }

    //Loads SignUpActivity
    private void loadSignUpActivity() {
        Intent intent =new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
        //Closes current activity
        finish();
    }
}

