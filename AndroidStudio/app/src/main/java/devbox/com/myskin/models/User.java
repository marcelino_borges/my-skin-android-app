package devbox.com.myskin.models;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Marcelino Borges on 11/06/2018 at 10:02.
 */
@IgnoreExtraProperties
public class User {
    private String name;
    private String id;
    private String email;
    private String password;
    private List<Reminder> remindersActive;
    private List<Reminder> remindersArchived;
    private ConfigsUser configs;
    private Bitmap pictureUserProfile;

    public User() {
    }

    //Without password
    public User(String name, String id, String email) {
        this.name = name;
        this.id = id;
        this.email = email;
        this.remindersActive = new LinkedList<>();
        this.configs = new ConfigsUser();

    }

    //With password
    public User(String name, String id, String email, String password) {
        this.name = name;
        this.id = id;
        this.email = email;
        this.remindersActive = new LinkedList<>();
        this.password = password;
    }

    @PropertyName("Name")
    public String getName() {
        return name;
    }

    @PropertyName("Name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("ID")
    public String getId() {
        return id;
    }

    @PropertyName("ID")
    public void setId(String id) {
        this.id = id;
    }

    @PropertyName("Email")
    public String getEmail() {
        return email;
    }

    @PropertyName("Email")
    public void setEmail(String email) {
        this.email = email;
    }

    @Exclude // Tells Firebase not to get the user password from the database
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @PropertyName("Reminders_Active")
    public List<Reminder> getRemindersActive() {
        return remindersActive;
    }

    @PropertyName("Reminders_Active")
    public void setRemindersActive(List<Reminder> remindersActive) {
        this.remindersActive = remindersActive;
    }

    @PropertyName("Reminders_Archived")
    public List<Reminder> getRemindersArchived() {
        return remindersArchived;
    }

    @PropertyName("Reminders_Archived")
    public void setRemindersArchived(List<Reminder> remindersArchived) {
        this.remindersArchived = remindersArchived;
    }

    @PropertyName("User_Configs")
    public ConfigsUser getConfigs() {
        return configs;
    }

    @PropertyName("User_Configs")
    public void setConfigs(ConfigsUser configs) {
        this.configs = configs;
    }

    @Exclude
    public Bitmap getPictureUserProfile() {
        return pictureUserProfile;
    }

    public void setPictureUserProfile(Bitmap pictureUserProfile) {
        this.pictureUserProfile = pictureUserProfile;
    }

    @Override
    public String toString() {
        return "UserID: " + id + "\nEmail: " + email + "\nName: " + name + "\nReminders active: " + remindersActive.size() + "\nReminders Archived: " + remindersArchived.size();
    }

    public void addReminder(Reminder newReminder) {
        if(remindersActive != null) {
            remindersActive.add(newReminder);
        }
    }
}

