package devbox.com.myskin.models;

import android.graphics.Bitmap;
import android.location.Location;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;
import java.util.LinkedHashMap;

/**
 * Created by Marcelino Borges on 11/06/2018 at 13:17.
 */
@IgnoreExtraProperties //Firebase ignores the incompatible data types
public class Reminder {
    private String title, alarmInterval, notes;
    /**
     * AlarmInterval unique possible values:
     * - daily
     * - weekday
     * - weekends
     * - none
     * - or a number (meaning a certain amount of hours)
     **/
    private DateTime firstDateTime;
    private boolean repeat;
    private Bitmap image;
    private LinkedHashMap<String, Location> locationsNotAlarm;
    private LinkedHashMap<String, Location> locationsDoAlarm;

    public Reminder() {
    }

    public Reminder(String title, DateTime firstDateTime) {
        this.title = title;
        this.firstDateTime = firstDateTime;
        this.alarmInterval = "";
        this.repeat = false;
        this.notes = "unknown";
        this.locationsDoAlarm = new LinkedHashMap<>();
        this.locationsNotAlarm = new LinkedHashMap<>();
    }

    public Reminder(String title, DateTime firstDateTime, String notes) {
        this.title = title;
        this.firstDateTime = firstDateTime;
        this.alarmInterval = "";
        this.repeat = false;
        this.notes = notes;
        this.locationsDoAlarm = new LinkedHashMap<>();
        this.locationsNotAlarm = new LinkedHashMap<>();
    }

    @PropertyName("Title")
    public String getTitle() {
        return title;
    }

    @PropertyName("Title")
    public void setTitle(String title) {
        this.title = title;
    }

    @PropertyName("Initial_Date")
    public DateTime getFirstDateTime() {
        return firstDateTime;
    }

    @PropertyName("Initial_Date")
    public void setFirstDateTime(DateTime firstDateTime) {
        this.firstDateTime = firstDateTime;
    }

    @PropertyName("Alarm_Interval")
    public String getAlarmInterval() {
        return alarmInterval;
    }

    @PropertyName("Alarm_Interval")
    public void setAlarmInterval(String alarmInterval) {
        this.alarmInterval = alarmInterval;
    }

    @PropertyName("Repeat")
    public boolean isRepeat() {
        return repeat;
    }

    @PropertyName("Repeat")
    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    @PropertyName("Notes")
    public String getNotes() {
        return notes;
    }

    @PropertyName("Notes")
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Exclude
    public Bitmap getImage() {
        return image;
    }

    //TODO: ver uma alternativa pro tipo Bitmap (Firebase não aceita). Talvez string com endereço da imagem ou a imagem convertida para vetor

    public void setImage(Bitmap image) {
        this.image = image;
    }

    @PropertyName("Locations_Not_Alarm")
    public LinkedHashMap<String, Location> getLocationsNotAlarm() {
        return locationsNotAlarm;
    }

    @PropertyName("Locations_Not_Alarm")
    public void setLocationsNotAlarm(LinkedHashMap<String, Location> locationsNotAlarm) {
        this.locationsNotAlarm = locationsNotAlarm;
    }

    @PropertyName("Locations_Do_Alarm")
    public LinkedHashMap<String, Location> getLocationsDoAlarm() {
        return locationsDoAlarm;
    }

    @PropertyName("Locations_Do_Alarm")
    public void setLocationsDoAlarm(LinkedHashMap<String, Location> locationsDoAlarm) {
        this.locationsDoAlarm = locationsDoAlarm;
    }
}
