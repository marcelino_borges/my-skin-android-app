package devbox.com.myskin.models;

import com.google.firebase.database.PropertyName;

/**
 * Created by Marcelino on 28/06/2018 at 12:33
 */
public class DateTime {
    private String day, month, year, hour, minute;

    public DateTime() {
    }

    public DateTime(String day, String month, String year, String hour, String minute) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.minute = minute;
    }

    @PropertyName("Day")
    public String getDay() {
        return day;
    }

    @PropertyName("Day")
    public void setDay(String day) {
        this.day = day;
    }

    @PropertyName("Month")
    public String getMonth() {
        return month;
    }

    @PropertyName("Month")
    public void setMonth(String month) {
        this.month = month;
    }

    @PropertyName("Year")
    public String getYear() {
        return year;
    }

    @PropertyName("Year")
    public void setYear(String year) {
        this.year = year;
    }

    @PropertyName("Hour")
    public String getHour() {
        return hour;
    }

    @PropertyName("Hour")
    public void setHour(String hour) {
        this.hour = hour;
    }

    @PropertyName("Minute")
    public String getMinute() {
        return minute;
    }

    @PropertyName("Minute")
    public void setMinute(String minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    }
}
