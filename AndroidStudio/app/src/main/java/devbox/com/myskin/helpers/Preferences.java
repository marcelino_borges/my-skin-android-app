package devbox.com.myskin.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Marcelino Borges on 11/06/2018 at 13:10.
 */

public class Preferences {
    private Context context;
    private SharedPreferences preferences;
    private final String FILE_NAME = "myskin.preferences";
    private final int MODE_PRIVATE = 0; // Context.MODE_PRIVATE = (int) 0
    private SharedPreferences.Editor editor;

    private static final String KEY_EMAIL = "email_current_user";
    private static final String KEY_UID = "id_current_user";
    private static final String KEY_IS_FIRST_ACCESS = "is_first_access";


    public Preferences(Context context) {
        this.context = context;

        preferences = this.context.getSharedPreferences(FILE_NAME, MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void saveUserInPreferences(String emailUserLoggedIn, String userId) {
        // Adding the data
        editor.putString(KEY_EMAIL, emailUserLoggedIn);
        editor.putString(KEY_UID, userId);
        // Saves and closes the file
        editor.commit();
    }

    public String getEmailInPreferences () {
        // Returns the id OR null parameter for the case the key is not found
        return preferences.getString(KEY_EMAIL,null);
    }

    public String getUserIdInPreferences() {
        // Returns the id OR null parameter for the case the key is not found
        return preferences.getString(KEY_UID,null);
    }

    public String isUserFirstAccess() {
        // Get to know if it is the first time accessing the app (if we have already accessed MainActivity,
        // it's not the first time anymore, so the value is false)
        return preferences.getString(KEY_IS_FIRST_ACCESS,null);
    }

    public void informFirstAccess() {
        // It WAS the first time the user is accessing the app and stores "false", so
        // the next time we'll inform that it will NOT be the first time anymore
        editor.putString(KEY_IS_FIRST_ACCESS, "false");
        //Saves and closes the file
        editor.commit();
    }
}
