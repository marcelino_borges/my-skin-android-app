package devbox.com.myskin.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import devbox.com.myskin.R;
import devbox.com.myskin.helpers.Preferences;

public class WelcomeScreen2Activity extends AppCompatActivity {

    private Button mButton_enter;
    private Preferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen2);

        mPreferences = new Preferences(this);

        mButton_enter = findViewById(R.id.btn_welcome_screen2_enter);

        mButton_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // After entering the app here (clicking the ENTER button), the enter button's click listener
                // stores that it's not anymore the first access of the user in the app
                loadMainActivity();
                mPreferences.informFirstAccess();
            }
        });
    }

    private void loadMainActivity() {
        startActivity(new Intent(WelcomeScreen2Activity.this, MainActivity.class));
    }
}
