package devbox.com.myskin.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import devbox.com.myskin.R;
import devbox.com.myskin.activities.MainActivity;
import devbox.com.myskin.activities.NewReminderActivity;
import devbox.com.myskin.activities.ReminderDetailsActivity;
import devbox.com.myskin.models.Reminder;
import devbox.com.myskin.models.User;

/**
 * Created by Marcelino Borges on 11/06/2018 at 13:01.
 */

public class ReminderAdapter extends RecyclerView.Adapter<ReminderAdapter.ViewHolderReminder> {

    private Context mContext;
    private DatabaseReference mRemindersReference;
    private ChildEventListener mChildEventListener;
    private final String NODE_DB_USERS = "users";
    private final String NODE_DB_REMINDERS_ACTIVE = "Reminders_Active";
    private final String NODE_DB_REMINDERS_ARCHIVED = "Reminders_Archived";

    private List<Reminder> mRemindersList = new LinkedList<>();
    private ArrayList<Integer> mColors_list_theme = new ArrayList<>();
    private ArrayList<Integer> mIndexesUsed = new ArrayList<>();

    /////////////////
    //    START    //
    // ViewHolder  //
    /////////////////

    public void removeListenerFirebase() {
        mRemindersReference.removeEventListener(mChildEventListener);
    }

    public static class ViewHolderReminder extends RecyclerView.ViewHolder {
        public TextView txtFirstLine;
        public TextView txtSecondLine;
        public TextView txtThirdLine;
        public LinearLayout item_linearLayout;

        public ViewHolderReminder(View itemView) {
            super(itemView);
            txtFirstLine = itemView.findViewById(R.id.first_line_box_reminder);
            txtSecondLine = itemView.findViewById(R.id.second_line_box_reminder);
            txtThirdLine = itemView.findViewById(R.id.third_line_box_reminder);
            item_linearLayout = itemView.findViewById(R.id.linearLayout_recyclerItem);
        }
    }

    /////////////////
    //     END     //
    // ViewHolder  //
    /////////////////

    public ReminderAdapter(final Context context, DatabaseReference remindersRef) {
        //Context where the adapter is being called from
        this.mContext = context;
        //Sets the reference of the user's reminders list
        this.mRemindersReference = remindersRef;
        mIndexesUsed.clear();

        //Creating and setting the arrayList of colors to be applied randomly to the items background
        //mColors_list_theme.add(R.color.dark_grey_theme);
        mColors_list_theme.add(R.color.light_pink_theme);
        mColors_list_theme.add(R.color.orange_theme);
        mColors_list_theme.add(R.color.purple_theme);
        mColors_list_theme.add(R.color.yellow_theme);
        mColors_list_theme.add(R.color.green_theme);
        mColors_list_theme.add(R.color.light_grey_theme);


        //Listener to the reminders list on Firebase  TODO: verificar se esse child listener está funcionando corretamento por inteiro (changed, removed, moved etc) - O add já tá testado
        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()) {
                    //this dataSnapshot (new reminder / child added) also has to be added to this Adapter's list
                    Reminder newReminder = dataSnapshot.getValue(Reminder.class);
                    //Adding the new reminder to this Adapter's list
                    mRemindersList.add(newReminder);
                    //Updates the Recycler View, notifying that a new item has been inserted to the list at the last position
                    notifyItemInserted(mRemindersList.size() - 1);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Reminder newReminder = dataSnapshot.getValue(Reminder.class);
                int reminderIndex;
                try {
                    reminderIndex = Integer.parseInt(dataSnapshot.getKey()); //TODO: Verificar se essa key corresponde realmente ao index do reminder na lista do usuário
                    mRemindersList.set(reminderIndex, newReminder);
                    //Updates the Recycler View, notifying that an item has been changed in the list
                    notifyItemChanged(reminderIndex);
                } catch (Exception e) {
                    Log.e("MY_SKIN ERROR: ", "Exceção ao fazer parseInt na key do reminder, no ReminderAdapter > Construtor > new ChildEventListener > onChildChanged");
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                int reminderIndex;
                try {
                    reminderIndex = Integer.parseInt(dataSnapshot.getKey()); //TODO: Verificar se essa key corresponde realmente ao index do reminder na lista do usuário
                    mRemindersList.remove(reminderIndex);
                    //Updates the Recycler View, notifying that an item has been removed from the list, at this index
                    notifyItemRemoved(reminderIndex);
                } catch (Exception e) {
                    Log.e("MY_SKIN ERROR: ", "Exceção ao fazer parseInt na key do reminder, no ReminderAdapter > Construtor > new ChildEventListener > onChildRemoved");
                }
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mContext, "Failed to load comments.", Toast.LENGTH_SHORT).show();
            }
        };

        mRemindersReference.addChildEventListener(mChildEventListener);

    }

    // Creates new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolderReminder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.reminders_recyclerview_item, parent,false);
        ViewHolderReminder viewHolderReminder = new ViewHolderReminder(view);

        return viewHolderReminder;
    }

    // Replaces the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolderReminder holder, int position) {
        if((mRemindersList != null) && (mRemindersList.size() > 0)) {
            //Generating a random number to be used as the index of the color's list
            Random random = new Random();

            int randomIndex = random.nextInt(mColors_list_theme.size() - 1);

//            int randomIndex;
//
//            do {
//                //Chooses a random index(color)
//                randomIndex = random.nextInt(mColors_list_theme.size() - 1);
//                //1. gera um número random
//                //2. confere se esse numero ja existe na lista de numeros usados e se essa lista já tem a mesma qtde de cores que a lista cores original
//                //3. se foi, volta para o 1
//                //4. se não foi, insere esse numero na lista de numeros usados
//                //5. seta o bg com o numero como indice da lista de cores original
//
//            } while (!mIndexesUsed.contains(randomIndex));
//
//            //As the code has already found a new color index, adds is to the list of used indexes
//            mIndexesUsed.add(randomIndex);

            //Setting the tint of the item's background
            holder.item_linearLayout.getBackground().setTint(mContext.getColor(mColors_list_theme.get(randomIndex)));

//            if(mIndexesUsed.size() >= mColors_list_theme.size() - 1) {
//                mIndexesUsed.clear();
//            }

            // gets the reminder from the list at this position
            Reminder reminder = mRemindersList.get(position);

            //Reminder's name
            holder.txtFirstLine.setText(reminder.getTitle());

            if(reminder.getFirstDateTime() != null) {
                holder.txtSecondLine.setText(reminder.getFirstDateTime().toString());

                //if the reminder is set to repeat, adds the proper text referring to that into the TextView
                if (reminder.isRepeat()) {
                    //Sets the proper repeat_interval text
                    switch (reminder.getAlarmInterval()) {
                        case "weekends":
                            holder.txtThirdLine.setText(R.string.new_reminder_freq_dialog_weekends);
                            break;
                        case "weekdays":
                            holder.txtThirdLine.setText(R.string.new_reminder_freq_dialog_weekdays);
                            break;
                        case "everyday":
                            holder.txtThirdLine.setText(R.string.new_reminder_freq_dialog_everyday);
                            break;
                        default:
                            //TODO: Crash aqui?
//                            Toast.makeText(mContext, reminder.getTitle() + " alarmInterval: " + reminder.getAlarmInterval(), Toast.LENGTH_SHORT).show();
//                            Log.e("MY_SKIN DEBUG: ", reminder.getTitle() + " alarmInterval: " + reminder.getAlarmInterval());
                            String repeatCustomHours = mContext.getString(R.string.new_reminder_freq_dialog_custom_pt1)
                                                        + " " + reminder.getAlarmInterval() + " "
                                                        + mContext.getString(R.string.new_reminder_freq_dialog_custom_pt2);
                            holder.txtThirdLine.setText(repeatCustomHours);
                            break;
                    }
                } else {
                    holder.txtThirdLine.setText(R.string.new_reminder_freq_dialog_no_repeat);
                }
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "click curto no item do recyclerView", Toast.LENGTH_SHORT).show();
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //TODO: mostrar diálogo com menu para apagar, exibir detalhes etc.
                //Toast.makeText(mContext, "click longo no item do recyclerView", Toast.LENGTH_SHORT).show();

                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);

                // Setting dialog list of items (EDIT, ARCHIVE, DELETE)
                String[] dialogItems = {mContext.getString(R.string.reminder_adapter_item_click_edit),
                                        mContext.getString(R.string.reminder_adapter_item_click_archive),
                                        mContext.getString(R.string.reminder_adapter_item_click_delete)};

                dialogBuilder.setItems(dialogItems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            //EDIT
                            case 0:
                                //When user clicks to edit the reminder, opens the ReminderDetailActivity
                                Intent reminderDetailIntent = new Intent(mContext, ReminderDetailsActivity.class);
                                //Sending the reminder's index in the recycler view list (same of the user's reminder's list stored in Firebase)
                                reminderDetailIntent.putExtra("reminder_list_position", holder.getAdapterPosition());
                                mContext.startActivity(reminderDetailIntent);
                                break;
                            //ARCHIVE
                            case 1:
                                //Sends the reminder to another list (archive)
                                //TODO: Criar outro node no firebase para armazenar os lembretes arquivados. Talvez outra lista dentro do User.

                                FirebaseAuth auth = FirebaseAuth.getInstance();

                                //Checks if user is logged in
                                if(auth.getCurrentUser() != null) {
                                    //Retrieving a new database reference from its root
                                    final DatabaseReference database = FirebaseDatabase.getInstance().getReference().child(NODE_DB_USERS).child(auth.getCurrentUser().getUid());

                                    database.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                final User userFound = dataSnapshot.getValue(User.class);
                                                final List<Reminder> remindersArchived;

                                                if(userFound.getRemindersArchived() == null) {
                                                    remindersArchived = new LinkedList<>();
                                                } else {
                                                    remindersArchived = userFound.getRemindersArchived();
                                                }

                                                //Building the confirmation dialog
                                                AlertDialog.Builder dialogConfirmationBuilder = new AlertDialog.Builder(mContext);
                                                dialogConfirmationBuilder.setMessage(mContext.getString(R.string.reminder_adapter_item_click_confirmation_archive_message));

                                                // Adding ARCHIVE button
                                                dialogConfirmationBuilder.setPositiveButton(R.string.reminder_adapter_item_click_archive, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        if(userFound.getRemindersActive() != null) {
                                                            List<Reminder> remindersActive = userFound.getRemindersActive();

                                                            //Adding the selected reminder to the Reminders_Archived list
                                                            remindersArchived.add(remindersActive.get(holder.getAdapterPosition()));
                                                            //Removing the selected reminder from the Reminders_Active list
                                                            remindersActive.remove(holder.getAdapterPosition());

                                                            //Saving the Reminders_Archived list in Firebase's user node
                                                            database.child(NODE_DB_REMINDERS_ARCHIVED).setValue(remindersArchived);

                                                            //Saving the Reminders_Active list in Firebase's user node and showing to user a confirmation message
                                                            database.child(NODE_DB_REMINDERS_ACTIVE).setValue(remindersActive).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    Toast.makeText(mContext, mContext.getString(R.string.main_activity_success_archive), Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                        } else {
                                                            Toast.makeText(mContext, mContext.getString(R.string.main_activity_reminders_active_not_found), Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                });

                                                // Adding CLOSE button
                                                dialogConfirmationBuilder.setNegativeButton(R.string.reminder_adapter_item_click_confirmation_delete_close, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                    }
                                                });

                                                dialogConfirmationBuilder.create();
                                                dialogConfirmationBuilder.show();
                                            } else {
                                                Log.e("MY_SKIN ERROR: ", "Usuário não localizado no Firebase > ReminderAdapter > OnBindViewHolder()");
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            Log.e("MY_SKIN ERROR: ", "Não foi possível encontrar a informação solicitada > ReminderAdapter.OnBindViewHolder().");
                                            Toast.makeText(mContext, mContext.getString(R.string.error_new_reminder_couldnt_find), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                break;
                            //DELETE
                            case 2:
                                FirebaseAuth authRef = FirebaseAuth.getInstance();
                                //Checks if user is logged in
                                if(authRef.getCurrentUser() != null) {
                                    //Retrieving a new database reference from its root
                                    final DatabaseReference database = FirebaseDatabase.getInstance().getReference().child(NODE_DB_USERS).child(authRef.getCurrentUser().getUid());

                                    database.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                User userFound = dataSnapshot.getValue(User.class);
                                                if(userFound.getRemindersActive() != null && userFound.getRemindersActive().size() > 0) {
                                                    final List<Reminder> reminders_list = userFound.getRemindersActive();

                                                    //Building the confirmation dialog
                                                    AlertDialog.Builder dialogConfirmationBuilder = new AlertDialog.Builder(mContext);
                                                    dialogConfirmationBuilder.setMessage(mContext.getString(R.string.reminder_adapter_item_click_confirmation_delete_message));

                                                    // Adding DELETE button
                                                    dialogConfirmationBuilder.setPositiveButton(R.string.reminder_adapter_item_click_delete, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            reminders_list.remove(holder.getAdapterPosition());

                                                            //Saving the Reminders_Active list and showing to user a confirmation message
                                                            database.child(NODE_DB_REMINDERS_ACTIVE).setValue(reminders_list).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    Toast.makeText(mContext, mContext.getString(R.string.main_activity_success_delete), Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                        }
                                                    });

                                                    // Adding CLOSE button
                                                    dialogConfirmationBuilder.setNegativeButton(R.string.reminder_adapter_item_click_confirmation_delete_close, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.dismiss();
                                                        }
                                                    });

                                                    dialogConfirmationBuilder.create();
                                                    dialogConfirmationBuilder.show();
                                                }
                                            } else {
                                                Log.e("MY_SKIN ERROR: ", "Usuário não localizado no Firebase > ReminderAdapter > OnBindViewHolder()");
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            Log.e("MY_SKIN ERROR: ", "Não foi possível encontrar a informação solicitada > ReminderAdapter.OnBindViewHolder().");
                                            Toast.makeText(mContext, mContext.getString(R.string.error_new_reminder_couldnt_find), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                break;
                            default:
                                break;
                        }
                        dialog.dismiss();
                    }
                });

                // Adding CLOSE button
                dialogBuilder.setNegativeButton(R.string.new_reminder_negative_btn, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                });

                // Creating and showing the AlertDialog
                dialogBuilder.create();
                dialogBuilder.show();

                return false;
            }
        });

        //TODO: Recuperar aqui o LinearLayout do xml do layout da linha do recycler view e mudar a cor do fundo aleatoriamente
        //TODO: Verificar se tem link para imagem e, se tiver, mostrar no lugar da imagem padrão do item

    }

    @Override
    public int getItemCount() {
        return mRemindersList.size();
    }
}