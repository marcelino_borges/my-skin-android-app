package devbox.com.myskin.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import devbox.com.myskin.BuildConfig;
import devbox.com.myskin.R;
import devbox.com.myskin.models.DateTime;
import devbox.com.myskin.models.Reminder;
import devbox.com.myskin.models.User;

public class ReminderDetailsActivity extends AppCompatActivity {

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private final String NODE_DB_USERS = "users";
    private final String NODE_DB_REMINDERS_ACTIVE = "Reminders_Active";

    //UI references
    private TextInputEditText mEditText_title, mEditText_notes, mEditText_repeatHours;
    private TextInputLayout mInputLayout_title, mInputLayout_date, mInputLayout_time;
    private TextView mTextView_RepeatField, mEditText_date, mEditText_time;
    private Switch mSwitch_repeat;
    private ImageButton mBtn_camera;
    private RadioGroup mRadioGroup_repeat;
    private ImageView mImageView_photoCamera, mBtn_back;
    private ConstraintLayout constraintLayout;

    private AlertDialog repeatDialog;

    //Reminder
    private String mTitleText, mNotesText, mRepeatText, mAlarmInterval;
    private boolean mIsRepeatSwitchOn;
    private Calendar mCalendar;
    private String mHour, mMinute, mYear, mMonth, mDay;
    private Reminder mEditedReminder;
    private boolean canDiscardPhoto = false;
    private int mReminderIndex;

    //Camera
    private static final int REQUEST_TAKE_PHOTO  = 1, SELECT_FILE = 2, REQUEST_PERM_WRITE_STORAGE = 3, REQUEST_PERM_CAMERA = 4;
    private Uri photoURI = null;
    private File photoFile = null;
    private String mCurrentPhotoPath = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        updateUiForms();

        //UI references
        /////Text Input Edit Texts
        mEditText_title = findViewById(R.id.edit_reminder_details_title);
        mEditText_notes = findViewById(R.id.edit_reminder_details_notes);
        mEditText_date = findViewById(R.id.edit_reminder_details_date);
        mEditText_time = findViewById(R.id.edit_reminder_details_time);
        mTextView_RepeatField = findViewById(R.id.edit_reminder_details_repeat);

        /////Text Input Layout
        mInputLayout_title = findViewById(R.id.input_layout_reminder_details_title);
        mInputLayout_date = findViewById(R.id.input_layout_reminder_details_date);
        mInputLayout_time = findViewById(R.id.input_layout_reminder_details_time);

        mImageView_photoCamera = findViewById(R.id.imageView_reminder_details);

        mSwitch_repeat = findViewById(R.id.reminder_details_switch_repeat);
        mBtn_back = findViewById(R.id.img_btn_back_reminder_details);
        mBtn_camera = findViewById(R.id.btnTakePicture_reminder_details);
        FloatingActionButton fab = findViewById(R.id.fab_reminder_details);
        constraintLayout = findViewById(R.id.masterConstraintLayout_reminder_details);

        //Switch starts off
        mIsRepeatSwitchOn = false;

        //Repeat field starts disabled
        mTextView_RepeatField.setEnabled(false);

        //Making Date and Time fields not editable
        mEditText_date.setKeyListener(null);
        mEditText_time.setKeyListener(null);

        mCalendar = Calendar.getInstance();

        //Listener to the Repeat Switch
        mSwitch_repeat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    mIsRepeatSwitchOn = true;
                    mTextView_RepeatField.setEnabled(true);
                    mTextView_RepeatField.setFocusable(true);
                    mTextView_RepeatField.requestFocus();
                } else {
                    mIsRepeatSwitchOn = false;
                    mTextView_RepeatField.setEnabled(false);
                    mTextView_RepeatField.setFocusable(false);
                }
            }
        });


        //Listener to the Repeat Edit Text (pops up a dialog box)
        mTextView_RepeatField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTextView_RepeatField.isEnabled()) {
                    repeatDialog = createRepeatDialog();
                }
            }
        });


        //Setting DatePickerDialog logic
        mEditText_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ReminderDetailsActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mYear = String.valueOf(year);

                        mMonth = (monthOfYear < 10) ? "0" + String.valueOf(monthOfYear) : String.valueOf(monthOfYear);

                        mDay = (dayOfMonth < 10) ? "0" + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);

                        String date = dayOfMonth + "/" + monthOfYear + "/" + year;

                        //Updates the date EditText with the picked date from the dialog
                        mEditText_date.setText(date);
                    }
                }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mEditText_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(ReminderDetailsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        mHour = (selectedHour < 10) ? "0" + String.valueOf(selectedHour) : String.valueOf(selectedHour);
                        mMinute = (selectedMinute < 10) ? "0" + String.valueOf(selectedMinute) : String.valueOf(selectedMinute);

                        String time = mHour + ":" + mMinute;
                        mEditText_time.setText(time);
                    }
                }, mCalendar.get(Calendar.HOUR_OF_DAY), mCalendar.get(Calendar.MINUTE), true).show();// 24 hour time
            }
        });

        //Floating Action Button listener
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(saveNewReminder()) {
                    loadMainActivity();
                }
            }
        });

        // ImageButton_Back listener
        mBtn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMainActivity();
            }
        });

        mBtn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImagemDialog();
            }
        });

        mImageView_photoCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //createDialogViewPhoto(mBitmapImageCamera);
                if(mImageView_photoCamera.getDrawable() != null) {
                    //TODO: implementar visualização da foto
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //if photoFile has an image and the user don't press to save the reminder, the photo's file will be deleted from the storage, avoiding to occupy unnecessary internal space
        if(photoFile != null && canDiscardPhoto) {
            photoFile.delete();
        }
    }

    ///////////////////////
    //                   //
    //  CUSTOM METHODS   // ___
    //                   //    |
    ///////////////////////    V


    private void updateUiForms() {
        mAuth = FirebaseAuth.getInstance();

        //Getting the reminder's index sent through the intent
        mReminderIndex = getIntent().getIntExtra("reminder_list_position", -1);

        if(mAuth.getCurrentUser() != null && mReminderIndex >= 0) {
            try {
                //Setting reference to the user's reminder's list
                mDatabase = FirebaseDatabase.getInstance().getReference().child(NODE_DB_USERS).child(mAuth.getCurrentUser().getUid()).child("Reminders_Active").child(String.valueOf(mReminderIndex));

                mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Reminder reminder = dataSnapshot.getValue(Reminder.class);

                            mTitleText = reminder.getTitle();
                            mNotesText = reminder.getNotes();
                            mDay = reminder.getFirstDateTime().getDay();
                            mMonth = reminder.getFirstDateTime().getMonth();
                            mYear = reminder.getFirstDateTime().getYear();
                            mHour = reminder.getFirstDateTime().getHour();
                            mMinute = reminder.getFirstDateTime().getMinute();

                            if (reminder.isRepeat()) {
                                //Sets the repeat_switch to off (false)
                                mIsRepeatSwitchOn = true;
                                mSwitch_repeat.setChecked(mIsRepeatSwitchOn);
                                mSwitch_repeat.setFocusable(mIsRepeatSwitchOn);
                                //Sets the proper repeat_interval text
                                switch (reminder.getAlarmInterval()) {
                                    case "weekends":
                                        mRepeatText = getString(R.string.new_reminder_freq_dialog_weekends);
                                        mTextView_RepeatField.setText(mRepeatText);
                                        break;
                                    case "weekdays":
                                        mRepeatText = getString(R.string.new_reminder_freq_dialog_weekdays);
                                        mTextView_RepeatField.setText(mRepeatText);
                                        break;
                                    case "everyday":
                                        mRepeatText = getString(R.string.new_reminder_freq_dialog_everyday);
                                        mTextView_RepeatField.setText(mRepeatText);
                                        break;
                                    default:
                                        mRepeatText = getString(R.string.new_reminder_freq_dialog_custom_pt1)
                                                                    + " " + reminder.getAlarmInterval() + " "
                                                                    + getString(R.string.new_reminder_freq_dialog_custom_pt2);
                                        mTextView_RepeatField.setText(mRepeatText);
                                        break;
                                }
                            } else {
                                //Set the field's text to "unique event"
                                mRepeatText = getString(R.string.new_reminder_freq_dialog_no_repeat);
                                mTextView_RepeatField.setText(mRepeatText);
                                //Sets the repeat_switch to off (false)
                                mIsRepeatSwitchOn = false;
                                mSwitch_repeat.setChecked(mIsRepeatSwitchOn);
                                mSwitch_repeat.setFocusable(mIsRepeatSwitchOn);
                            }

                            mEditText_title.setText(mTitleText);
                            mEditText_notes.setText(mNotesText);
                            String date = mDay + "/" + mMonth + "/" + mYear;
                            mEditText_date.setText(date);
                            String time = mHour + ":" + mMinute;
                            mEditText_time.setText(time);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        showToast(getString(R.string.reminder_details_error_couldnt_find));
                        Log.e("MY_SKIN ERROR: ", "Não foi possível encontrar a informação solicitada na NewReminderActivity.findUserInDatabase().");
                    }
                });
            } catch (NullPointerException npe) {
                Log.e("MY_SKIN ERROR: ", "Não foi possível carregar os dados do Reminder, voltando para a MainActivity - Local: ReminderDetailsActivity");
                //Showing error message to user
                showToast(getString(R.string.reminder_details_error_index_not_found));
                //Returning to MainActivity
                startActivity(new Intent(this, MainActivity.class));
                this.finish();
            }
        }
    }

    private boolean saveNewReminder() {
        mTitleText = capitalizeFirstLetter(mEditText_title.getText().toString());
        mNotesText = mEditText_notes.getText().toString();
        mRepeatText = mTextView_RepeatField.getText().toString();

        DateTime dateTime = new DateTime(mDay, mMonth, mYear, mHour, mMinute);

        mEditedReminder = new Reminder(mTitleText,dateTime);

        // Checks if Title, Date and Time (required) fields have text
        if (validateRequiredFields()) {
            //If mNotesText has text, saves its content to the object Reminder
            if(hasData(mNotesText)) {
                mEditedReminder.setNotes(mNotesText);
            }
            //If repeat switch is on, then saves the repeat editText's content
            if (mIsRepeatSwitchOn && hasData(mRepeatText)) {
                mEditedReminder.setAlarmInterval(mAlarmInterval);
                mEditedReminder.setRepeat(true);
            }

            if (!mIsRepeatSwitchOn) {
                mEditedReminder.setAlarmInterval("none");
                mEditedReminder.setRepeat(false);
            }
            //If it has a bitmap, sets it to the newReminder
            /* TODO: ver uma alternativa pro tipo Bitmap (Firebase não aceita). Talvez string com endereço da imagem no Firebase Storage ou a imagem convertida para vetor
            if(mBitmapImageCaptured != null) {
                newReminder.setmReminderImage(mBitmapImageCaptured);
            }*/

            //Checks if we really have a Reminder's object before trying to save it
            if(mEditedReminder != null) {
                replaceReminderInDatabase();
            }

            canDiscardPhoto = false;

            return true;
        } else {
            //Inform user about the missing required fields
            Snackbar.make(getWindow().getDecorView().getRootView(), getString(R.string.error_new_reminder_required_fields), Snackbar.LENGTH_LONG).setAction("OK", null).show();

            //Checks if all required fields have text
            if(!hasData(mTitleText)) {
                mEditText_title.setError(getString(R.string.error_field_required));
                mInputLayout_title.setError(getString(R.string.error_field_required));
            }
            if(!hasData(mEditText_date.getText().toString())) {
                mEditText_date.setError(getString(R.string.error_field_required));
                mInputLayout_date.setError(getString(R.string.error_field_required));
            }
            if(!hasData(mEditText_time.getText().toString())) {
                mEditText_time.setError(getString(R.string.error_field_required));
                mInputLayout_time.setError(getString(R.string.error_field_required));
            }
            return false;
        }
    }

    private void replaceReminderInDatabase() {
        mAuth = FirebaseAuth.getInstance();

        //Checks if user is logged in
        if(mAuth.getCurrentUser() != null) {
            //Retrieving a new database reference from its root
            mDatabase = FirebaseDatabase.getInstance().getReference().child(NODE_DB_USERS).child(mAuth.getCurrentUser().getUid()).child(NODE_DB_REMINDERS_ACTIVE).child(String.valueOf(mReminderIndex));

            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        mDatabase.setValue(mEditedReminder).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                showToast(getString(R.string.reminder_details_success_edit));
                            }
                        });
                    } else {
                        Log.e("MY_SKIN ERROR: ", "O reminder não foi localizado no Firebase");
                        showToast(getString(R.string.reminder_details_error_reminder_not_found));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    showToast(getString(R.string.error_new_reminder_couldnt_find));
                    Log.e("MY_SKIN ERROR: ", "Não foi possível encontrar a informação solicitada na NewReminderActivity.findUserInDatabase().");
                }
            });
        }
    }

    private void showToast(String msg) {
        Toast.makeText(ReminderDetailsActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    private void showIndefiniteSnackBar(String msg) {
        Snackbar.make(constraintLayout, msg, Snackbar.LENGTH_INDEFINITE).setAction(R.string.new_reminder_snackbar_ok, null).setActionTextColor(Color.YELLOW).show();
    }

    private String capitalizeFirstLetter(String name) {
        if(name == null || name.length() == 0)
            return name;

        return name.substring(0, 1).toUpperCase() + name.substring(1,name.length());
    }

    private boolean hasData(String text) {
        return !TextUtils.isEmpty(text);
    }

    private boolean validateRequiredFields() {
        //Checks if the needed fields are filled
        if (mTitleText.isEmpty() || mTitleText == null) {
            mInputLayout_title.setError(getString(R.string.error_field_required));
            mEditText_title.setError(getString(R.string.error_field_required));
            return false;
        } else if (mEditText_date.getText().toString().isEmpty() || mEditText_date.getText().toString() == null) {
            mInputLayout_time.setError(getString(R.string.error_field_required));
            mEditText_date.setError(getString(R.string.error_field_required));
            return false;
        } else if (mEditText_time.getText().toString().isEmpty() || mEditText_time.getText().toString() == null) {
            mInputLayout_time.setError(getString(R.string.error_field_required));
            mEditText_time.setError(getString(R.string.error_field_required));
            return false;
        } else {
            return true;
        }
    }

    public boolean isUserLoggedIn() {
        // Sets up Firebase Authentication instance
        mAuth = FirebaseAuth.getInstance();

        // Retrieving a possible logged-in user
        FirebaseUser currentUser = mAuth.getCurrentUser();

        return (currentUser != null);

    }

    //////////////////////
    //                  //
    //  CAMERA SECTION  // ___
    //                  //    |
    //////////////////////    V

    private void selectImagemDialog() {
        final CharSequence[] items = {getString(R.string.new_reminder_camera_text), getString(R.string.new_reminder_gallery_text), getString(R.string.new_reminder_cancel_text)};

        AlertDialog.Builder builder = new AlertDialog.Builder(ReminderDetailsActivity.this);
        builder.setTitle("Selecionar imagem");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(items[which].equals(getString(R.string.new_reminder_camera_text))) {

                    //Checks if the device has a camera before calling the method to open it
                    if(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                        //Checks if there is permission to use the camera, if not, requests it to the user
                        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ReminderDetailsActivity.this, new String[] {Manifest.permission.CAMERA}, REQUEST_PERM_CAMERA);
                        } else {
                            dispatchTakePictureIntent();
                        }
                    } else {
                        showToast(getString(R.string.error_no_camera));
                    }

                } else if (items[which].equals(getString(R.string.new_reminder_gallery_text))) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, getString(R.string.new_reminder_select_file)), SELECT_FILE);

                } else if (items[which].equals(getString(R.string.new_reminder_cancel_text))) {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        String imageFileName = "IMG_REMINDER_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",   /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            //photoFile = null;
            try {
                //Asks user permission to write-storage
                if(ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERM_WRITE_STORAGE);
                }
                //creates the file and saves it
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d("MY_SKIN DEBUG: ", "try-catch - IOException: dispatchTakePictureIntent() > photoFile = createImageFile();");
                return;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(ReminderDetailsActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            mImageView_photoCamera.setImageBitmap(bitmap);
        }
        galleryAddPic();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Treating user's denial choice to the camera permission
        if(requestCode == REQUEST_PERM_CAMERA) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                //Shows toast to the user informing he cant take a picture to the reminder without the camera permission
                showIndefiniteSnackBar(getString(R.string.new_reminder_nocamera_perm));
            }
            //Treating user's denial choice to the Write_Storage permission
        } else if(requestCode == REQUEST_PERM_WRITE_STORAGE) {
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_DENIED) {
                //Shows toast to the user informing he cant take a picture to the reminder without the camera permission
                showIndefiniteSnackBar(getString(R.string.new_reminder_nocamera_perm));
            }
        }
    }

    //////////////////////
    //                  //
    //      DIALOGS     // ___
    //                  //    |
    //////////////////////    V

    private AlertDialog createRepeatDialog() {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(ReminderDetailsActivity.this);
        // Set other dialog properties
        builder.setView(R.layout.dialog_pick_repetition);

        // Add the buttons
        builder.setPositiveButton(R.string.new_reminder_positive_btn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (repeatDialog != null) {
                    mEditText_repeatHours = repeatDialog.findViewById(R.id.edit_freq_dialog_custom_hours); //EditText inside dialog

                    mRadioGroup_repeat = repeatDialog.findViewById(R.id.new_reminder_freq_dialog_radioGroup);

                    //If the user clicks the editText of custom repeat hours, it's radio button gets automatically checked
                    mEditText_repeatHours.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RadioButton customHoursRadioButton = repeatDialog.findViewById(R.id.radiob_freq_dialog_custom);
                            customHoursRadioButton.setChecked(true);
                        }
                    });

                    String finalTextRepeat;

                    //Finding the id of the selected radio button
                    int radioButtonSelectedId = mRadioGroup_repeat.getCheckedRadioButtonId();
                    String radioButtonText = "";

                    if(radioButtonSelectedId != 0) {
                        //Getting the text of the selected radio button
                        radioButtonText = ((RadioButton) repeatDialog.findViewById(radioButtonSelectedId)).getText().toString();
                    }

                    //Sets the string finalTextRepeat according to the text of the selected radio button
                    if (radioButtonSelectedId == R.id.radiob_freq_dialog_custom) {
                        //If user selects the option for a specific amount of hours
                        finalTextRepeat = getString(R.string.new_reminder_freq_dialog_custom_pt1) + " " + mEditText_repeatHours.getText().toString() + " " + getString(R.string.new_reminder_freq_dialog_custom_pt2);
                        //Converting to integer the alarm interval from the string retrieved from the EditText field
                        mAlarmInterval = mEditText_repeatHours.getText().toString();
                    } else {
                        finalTextRepeat = getString(R.string.reminder_repeat) + " " + radioButtonText.toLowerCase();
                        if (radioButtonSelectedId == R.id.radiob_freq_dialog_custom_everyday) {
                            mAlarmInterval = "everyday";
                        } else if (radioButtonSelectedId == R.id.radiob_freq_dialog_custom_weekdays) {
                            //TODO: Ver como fazer essa parte de dias de semana
                            mAlarmInterval = "weekdays";
                        } else if (radioButtonSelectedId == R.id.radiob_freq_dialog_custom_weekends) {
                            //TODO: Ver como fazer essa parte de fins de semana
                            mAlarmInterval = "weekends";
                        }
                    }

                    //sets the repeat editText of the NewReminderActivity
                    mTextView_RepeatField.setText(finalTextRepeat);

                    dialog.dismiss();
                }
            }
        });
        builder.setNegativeButton(R.string.new_reminder_negative_btn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();

            }
        });

        // Creates the AlertDialog
        alertDialog = builder.create();

        repeatDialog = alertDialog;

        alertDialog.show();

        if(repeatDialog != null) {
            //Setting everyday RadioButton as checked as soon as dialog shows up
            RadioButton everydayRadioButton = repeatDialog.findViewById(R.id.radiob_freq_dialog_custom_everyday);
            everydayRadioButton.setChecked(true);
        }

        return alertDialog;
    }

    private void showProgressDialog() {
        setContentView(R.layout.loading_screen);
//        AlertDialog.Builder builder = new AlertDialog.Builder(NewReminderActivity.this);
//        builder.setView(R.layout.loading_screen);
//        progressDialog = builder.create();
//        progressDialog.show();
    }

//    private void hideProgressBar() {
//        progressDialog.dismiss();
//    }

    ///////////////////////
    //                   //
    //  LOAD ACTIVITIES  // ___
    //                   //    |
    ///////////////////////    V

    private void loadLoginActivity() {
        startActivity(new Intent(ReminderDetailsActivity.this, LoginActivity.class));
        //Closes current activity
        finish();
    }

    private void loadMainActivity() {
        startActivity(new Intent(ReminderDetailsActivity.this, MainActivity.class));
        //Closes current activity
        finish();
    }

    //////////////////////
    //                  //
    //      MENUS       // ___
    //                  //    |
    //////////////////////    V

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_settings) {
            return true;
        } else if (id == R.id.menu_logout) {
            //Calls FirebaseAuth sign out method
            logUserOut();
            loadLoginActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    public void logUserOut () {
        if(isUserLoggedIn()) {
            mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
        }
    }

}
