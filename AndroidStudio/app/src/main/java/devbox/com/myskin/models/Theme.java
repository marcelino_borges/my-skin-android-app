package devbox.com.myskin.models;

import android.graphics.Color;

/**
 * Created by Marcelino Borges on 11/06/2018 at 13:20.
 */


public class Theme {
    private Color backGroundColor;
    private Color fontColor;

    public Theme(Color backGroundColor, Color fontColor) {
        this.backGroundColor = backGroundColor;
        this.fontColor = fontColor;
    }

    public Theme() {
    }

    public Color getBackGroundColor() {
        return backGroundColor;
    }

    public void setBackGroundColor(Color backGroundColor) {
        this.backGroundColor = backGroundColor;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }
}
