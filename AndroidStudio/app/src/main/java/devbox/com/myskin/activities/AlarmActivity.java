package devbox.com.myskin.activities;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

import devbox.com.myskin.R;
import devbox.com.myskin.helpers.ReminderReceiver;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class AlarmActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    private Button mDismiss_btn, mSleep_btn;
    private TextView mAlarmTitle_TextView;
    private EditText mAlarmNotes_EditText;
    private Uri notification;
    private MediaPlayer mediaPlayer;
    private final String REMINDER_TITLE_KEY = "REMINDER_TITLE";
    private final String REMINDER_NOTES_KEY = "REMINDER_NOTES";
    int optionSleepClicked = -1;
    AlarmManager alarmMgr;
    PendingIntent alarmIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alarm);

        //Vibration configuration
        int vibrationTime = 5; //Time seconds
        final Vibrator vibrator = (Vibrator)this.getSystemService(AlarmActivity.VIBRATOR_SERVICE);

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        mDismiss_btn = findViewById(R.id.alarmscreen_dismiss_btn);
        mSleep_btn = findViewById(R.id.alarmscreen_sleep_btn);
        mAlarmTitle_TextView = findViewById(R.id.alarm_screen_reminder_name);
        mAlarmNotes_EditText = findViewById(R.id.alarm_screen_reminder_notes);

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        mDismiss_btn.setOnTouchListener(mDelayHideTouchListener);
        mSleep_btn.setOnTouchListener(mDelayHideTouchListener);

        mDismiss_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer != null && mediaPlayer.isPlaying())
                    mediaPlayer.stop();
                finish();
            }
        });

        mSleep_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Finalizar lógica do sleep
                if(mediaPlayer != null && mediaPlayer.isPlaying())
                    mediaPlayer.stop();

                createDialogPickSleepTime();
            }
        });

        //Vibrate the phone
        try {
            vibrator.vibrate(vibrationTime * 1000);
        } catch (NullPointerException npe) {
            Log.e("MY SKIN DEBUG: ", "NullPointException at activity AlarmActivity - Vibrate");
        }

        if(savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                mAlarmTitle_TextView.setText(R.string.alarm_screen_error);
                mAlarmNotes_EditText.setText(R.string.alarm_screen_error);
            } else {
                mAlarmTitle_TextView.setText(extras.getString(REMINDER_TITLE_KEY));
                mAlarmNotes_EditText.setText(extras.getString(REMINDER_NOTES_KEY));
            }
        } else {
            String title = (String) savedInstanceState.getSerializable(REMINDER_TITLE_KEY);
            String notes = (String) savedInstanceState.getSerializable(REMINDER_NOTES_KEY);
            mAlarmTitle_TextView.setText(title);
            mAlarmNotes_EditText.setText(notes);
        }

        playAlarmSound();
    }

    //Dialog where the user can pick the time to delay the alarm (sleep function)
    private int createDialogPickSleepTime() {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialog);
        //List with time options available to the user choose in the dialog and delay the alarm
        String sleepOptions[]  = {
                getResources().getString(R.string.alarm_screen_sleep_1min),
                getResources().getString(R.string.alarm_screen_sleep_5min),
                getResources().getString(R.string.alarm_screen_sleep_15min),
                getResources().getString(R.string.alarm_screen_sleep_30min),
                getResources().getString(R.string.alarm_screen_sleep_60min)
        };
        //Setting the dialog title text
        builder.setTitle(R.string.alarm_screen_sleep_dialog);
        //Setting the list of items (amounts of time to delay the alarm), along with the choice's Listener

        alarmMgr = (AlarmManager) AlarmActivity.this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(AlarmActivity.this, ReminderReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(AlarmActivity.this, 0, intent, 0);

        builder.setItems(sleepOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        optionSleepClicked = 1;
                    case 1:
                        optionSleepClicked = 5;
                    case 2:
                        optionSleepClicked = 15;
                    case 3:
                        optionSleepClicked = 30;
                    case 4:
                        optionSleepClicked = 60;
                }

                //TODO: Fazer funcionar o sleep
                alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                        SystemClock.elapsedRealtime() +
                                optionSleepClicked * 60 * 1000, alarmIntent);

                finish();
            }
        });

        // Creates and show the AlertDialog
        alertDialog = builder.create();
        alertDialog.show();

        //returning the sleep-time option picked
        return optionSleepClicked;
    }

    private void playAlarmSound() {
        notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        if(notification == null){
            // if notification sound is null, tries another
            notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            if(notification == null) {
                //If still null, tries one more
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }

        if(notification == null) {
            Log.e("MY SKIN ERROR: ", "Media player null");
        } else {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), notification);
            mediaPlayer.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.stop();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
