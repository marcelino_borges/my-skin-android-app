package devbox.com.myskin.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;

import devbox.com.myskin.R;
import devbox.com.myskin.activities.AlarmActivity;

public class ReminderReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        final String REMINDER_TITLE_KEY = "REMINDER_TITLE";
        final String REMINDER_NOTES_KEY = "REMINDER_NOTES";

        Bundle extras = intent.getExtras();
        String alarmTitle, alarmNotes;

        if (extras == null) {
            alarmTitle = context.getResources().getString(R.string.alarm_screen_error);
            alarmNotes = context.getResources().getString(R.string.alarm_screen_error);
        } else {
            alarmTitle = extras.getString(REMINDER_TITLE_KEY);
            alarmNotes = extras.getString(REMINDER_NOTES_KEY);
        }

        Intent alarmScreenIntent = new Intent(context, AlarmActivity.class);

        alarmScreenIntent.putExtra(REMINDER_TITLE_KEY,alarmTitle);
        alarmScreenIntent.putExtra(REMINDER_NOTES_KEY,alarmNotes);

        //Opens the alarm screen
        context.startActivity(alarmScreenIntent);
    }
}
