package devbox.com.myskin.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import devbox.com.myskin.R;
import devbox.com.myskin.helpers.Preferences;

public class WelcomeScreenActivity extends AppCompatActivity {

    private Button mButton_start_tutorial, mButton_enter;
    private Preferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen1);

        mPreferences = new Preferences(this);

        //if we receive "false" from Preferences (then it's NOT the first access), opens MainActivity
        if(mPreferences.isUserFirstAccess() != null) {
            if (mPreferences.isUserFirstAccess().equalsIgnoreCase("false")) {
                loadMainActivity();
            } //if we receive other thing ("true" or null), continues in the the welcome screen (code below)
        }

        mButton_start_tutorial = findViewById(R.id.btn_welcome_screen_learn);
        mButton_enter = findViewById(R.id.btn_welcome_screen_enter);

        mButton_start_tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //If the user wants to learn how to use the app, we show an auto-descriptive image of the app
                loadWelcomeScreen2();
            }
        });

        mButton_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // After entering the app here (clicking the ENTER button), the enter button's click listener
                // stores that it's not anymore the first access of the user in the app
                loadMainActivity();
                mPreferences.informFirstAccess();
            }
        });
    }

    private void loadMainActivity() {
        startActivity(new Intent(WelcomeScreenActivity.this, MainActivity.class));
    }

    private void loadWelcomeScreen2() {
        startActivity(new Intent(WelcomeScreenActivity.this, WelcomeScreen2Activity.class));
    }
}
