package devbox.com.myskin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import devbox.com.myskin.R;
import devbox.com.myskin.adapters.ReminderAdapter;
import devbox.com.myskin.models.User;

/**
 * Created by Marcelino Borges on 10/06/2018 at 12:20.
 */

public class MainActivity extends AppCompatActivity {

    //UI

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUserRemindersRef;
    private final String NODE_DB_USERS = "users";
    private final String NODE_DB_REMINDERS_ACTIVE = "Reminders_Active";
    private ValueEventListener valueEventListener;

    //RecyclerView
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.OnItemTouchListener mReminderClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ////////////////////
        //    FIREBASE    //
        ////////////////////

        if(isUserLoggedIn()) {
            //Retrieving a new database reference from its root
            mDatabaseUserRemindersRef = FirebaseDatabase.getInstance().getReference().child(NODE_DB_USERS).child(getUID()).child(NODE_DB_REMINDERS_ACTIVE);
            mDatabaseUserRemindersRef.keepSynced(true);
        }

        //TODO: colocar na SettingsActivity opção para limpar arquivos da pasta local e opção para limpar arquivos não mais utilizados
        //TODO: Criar activity para exibir detalhes do Reminder
        //TODO: Colocar no menu mais 1 item para visualizar os reminders arquivados e poder reativar
        //TODO: Colocar opção na SettingsActivity para limpar os lembretes arquivados
        //TODO: Criar perfil da usuária ao criar o cadastro. Perguntar se usa maquiagem diariamente etc. Se usar, confirmar horário em que a usuária quer ser lembrada de retirar a maquiagem e criar lembrete automático e mudar carinha (de espanto) a cada "soneca"


        ///////////////////
        // RECYCLER VIEW //
        ///////////////////

        mRecyclerView = findViewById(R.id.recycler_view_reminders);
        //changes in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        //uses a linear layout manager
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Floating Action Button - ADD REMINDER
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBar();
                loadNewReminderActivity();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        //In case the currentUser gets here and is not logged-in. for safety, does this check
        if(!isUserLoggedIn()) {
            loadLoginActivity();
        } else {
            //Sets the adapter with the list retrieved from database
//            updateViewWithUserReminders();
            //Specifies an adapter to the Recycler View
            mAdapter = new ReminderAdapter(this, mDatabaseUserRemindersRef);
            mRecyclerView.setAdapter(mAdapter);
        }

//        mDatabaseUserRemindersRef.addValueEventListener(valueEventListener);


        //Click listeners into mAdapter.ViewHolder
    }

    @Override
    protected void onStop() {
        super.onStop();

        //TODO: ver como remover o listener do firebase no adapter
//        if(valueEventListener != null) {
//            mDatabaseUserRemindersRef.removeEventListener(valueEventListener);
//        }

        // Clean up comments listener



    }

    ///////////////////////
    //                   //
    //  CUSTOM METHODS   // ___
    //                   //    |
    ///////////////////////    V

//    public void updateViewWithUserReminders() {
//        valueEventListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                //Clears the local list before updating its data
//                mRemindersList.clear();
//
//                //TODO: Terminar essa parte. Ta dando crash no app. No ReminderAdapter.getItemCount()
//
//                if(dataSnapshot.exists()) {
//                    //Accesses the user's list of reminders in database and saves each of them to the local a list
//                    for (DataSnapshot data : dataSnapshot.getChildren()) {
//                        Reminder reminder = data.getValue(Reminder.class);
//                        DateTime dateTime = reminder.getFirstDateTime();
//                        mRemindersList.add(reminder);
//                    }
//                    mAdapter.notifyDataSetChanged();
////                      //Saving instance of the user object retrieved from the database as a member of the activity
////                  User userFound = dataSnapshot.getValue(User.class);
////                  if (userFound != null && FirebaseAuth.getInstance().getCurrentUser() != null) {
////                      if(userFound.getRemindersActive() != null) {
////
////                      }
////                  } else {
////                      Log.e("MY_SKIN ERROR: ", "Null User retrieved from Firebase Database");
////                  }
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                showToast(getString(R.string.error_new_reminder_couldnt_find));
//                Log.e("MY_SKIN ERROR: ", "Couldn't find the information queried from MainActivity.updateViewWithUserReminders().");
//            }
//        };
//    }

    private String getUID() {
        if(FirebaseAuth.getInstance().getCurrentUser() != null) {
            return FirebaseAuth.getInstance().getCurrentUser().getUid();
        } else {
            return null;
        }
    }

    private void showToast(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    public void logUserOut () {
        if(isUserLoggedIn()) {
            mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
        }
    }

    public boolean isUserLoggedIn() {
        // Sets up Firebase Authentication instance
        mAuth = FirebaseAuth.getInstance();

        // Retrieving a possible logged-in user
        FirebaseUser currentUser = mAuth.getCurrentUser();

        return (currentUser != null);
    }

    private void showProgressBar() {
        setContentView(R.layout.loading_screen);
    }

    ///////////////////////
    //                   //
    //  LOAD ACTIVITIES  // ___
    //                   //    |
    ///////////////////////    V

    private void loadLoginActivity() {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        //Closes current activity
        finish();
    }

    private void loadNewReminderActivity() {
        startActivity(new Intent(MainActivity.this, NewReminderActivity.class));
    }

    private void loadSettingsActivity() {
        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
    }

    ///////////////////////
    //                   //
    //    MENU TOOLBAR   // ___
    //                   //    |
    ///////////////////////    V

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        if(isUserLoggedIn()) {
            mAuth = FirebaseAuth.getInstance();
            final DatabaseReference database = FirebaseDatabase.getInstance().getReference();
            //Finding current user's name in database
            database.child(NODE_DB_USERS).child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //Gets positive result?
                    if(dataSnapshot.exists()) {
                        //Tries to cast to user object saved in database
                        User user = dataSnapshot.getValue(User.class);
                        //If this user casted is not null
                        if(user != null) {
                            //Accessing the first menu item (with the user's name)
                            MenuItem menu_username = menu.findItem(R.id.menu_username);
                            menu_username.setEnabled(false);
                            //Sets the item text with the user's name
                            menu_username.setTitle(getString(R.string.menu_username_prefix) + " " + user.getName());
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id) {
        case R.id.menu_settings:
            //Opens up the Settings Activity
            loadSettingsActivity();
            return true;
        case R.id.menu_logout:
            //Calls FirebaseAuth sign out method
            logUserOut();
            //Opens up the Login Activity
            loadLoginActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
