##DESCRIPTION
My Skin is an android app with women as the target audience.
It's objective is to present a friendly and easy UI where the 
user can find a simple and intuitive way to treat her skin 
with related medicines. 

The app will work with customizable reminders (alarms), which can keep an 
specific picture taken by the user and alarm in the specified date-time.

Reminders can also alarm according to user's location, with or without repeatition.

##AUTHOR
Marcelino Borges

##COLLABORATOR
David Ferreira (Diagrams revision)